#!/usr/bin/php 
<?php
if ($argc != 2)
    echo "Incorrect Parameters\n";
else
{
    $str = trim(preg_replace('/\s+/', ' ', $argv[1]));
    if (strchr($str, '+') != NULL)
    {
        $arr = explode("+", $str);
        $str1 = trim($arr[0]);
        $str2 = trim($arr[1]);
        if (is_numeric($str1) && is_numeric($str2))
            echo $str1 + $str2."\n";
        else
            echo "Syntax Error"."\n";
    }
    else if (strchr($str, '-') != NULL)
    {
        $arr = explode("-", $str);
        $str1 = trim($arr[0]);
        $str2 = trim($arr[1]);
        if (is_numeric($str1) && is_numeric($str2))
            echo $str1 - $str2."\n";
        else
            echo "Syntax Error"."\n";
    }
    else if (strchr($str, '*') != NULL)
    {
        $arr = explode("*", $str);
        $str1 = trim($arr[0]);
        $str2 = trim($arr[1]);
        if (is_numeric($str1) && is_numeric($str2))
            echo $str1 * $str2."\n";
        else
            echo "Syntax Error"."\n";
    }
    else if (strchr($str, '/') != NULL)
    {
        $arr = explode("/", $str);
        $str1 = trim($arr[0]);
        $str2 = trim($arr[1]);
        if (is_numeric($str1) && is_numeric($str2))
            echo $str1 / $str2."\n";
        else
            echo "Syntax Error"."\n";
    }
    else if (strchr($str, '%') != NULL)
    {
        $arr = explode("%", $str);
        $str1 = trim($arr[0]);
        $str2 = trim($arr[1]);
        if (is_numeric($str1) && is_numeric($str2))
            echo $str1 % $str2."\n";
        else
            echo "Syntax Error"."\n";
    }
    else
        echo "Syntax Error"."\n";
}
?>