#!/usr/bin/php 
<?php
if ($argc != 4)
    echo "Incorrect Parameters\n";
else
{
    $arithm = trim($argv[2]);
    $num1 = trim($argv[1]);
    $num2 = trim($argv[3]);
    if ($arithm == '+')
        echo $num1 + $num2."\n";
    else if ($arithm == '-')
        echo $num1 - $num2."\n";
    else if ($arithm == '*')
        echo $num1 * $num2."\n";
    else if ($arithm == '/')
        echo $num1 / $num2."\n";
    else if ($arithm == '%')
        echo $num1 % $num2."\n";
}
?>