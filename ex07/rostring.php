#!/usr/bin/php
<?php
if ($argc > 1)
{
$str = preg_replace('/\s+/', " ", $argv[1]);
$str = trim($str);
$arr = explode(" ", $str);
$results = count($arr);
if ($results == 1)
    echo $arr[0]."\n";
else
{
    $i = 1;
    while ($i < $results)
    {
        echo $arr[$i]." ";
        $i++;        
    }
    echo $arr[0]."\n";
}
}
?>