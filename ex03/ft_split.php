<?php
function ft_split($str)
{
	$str1 = trim(preg_replace('/\s+/', ' ', $str));
	$arr = explode(" ", $str1);
	sort($arr, SORT_STRING);
	return $arr;
}
?>