#!/usr/bin/php
<?php

$loop = 1;
while ($loop)
{
	print("Enter a number: ");
	if ($str = fgets(STDIN))
	{
		$cmd = rtrim($str, "\n");
		if (is_numeric($cmd))
		{
			$ret = intval($cmd);
			if ($ret % 2 == 0)
				echo "The number {$cmd} is even", PHP_EOL;
			else
				echo "The number {$cmd} is odd", PHP_EOL;
		}
		else
			echo "'{$cmd}' is not a number", PHP_EOL;
	}
	else
	{
		echo "\n";
		$loop = 0;
	}
}
?>